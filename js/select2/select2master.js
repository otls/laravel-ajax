/**
 * selectAnOption: check if it has pre-defined value
 * @param {object} select
 * @return {void}
 */
const selectAnOption = (select) => {
    let dataValue = select.attr('data-value');
    let id = select.attr('data-id');

    if (id && dataValue) {
        let data = {
            [id]: dataValue
        };

        let query = {
            filters: JSON.stringify(data)
        }

        $.ajax({
            type: 'GET',
            url: select.dataSource + "/selected",
            data: query,
            dataType: "json",
            success: function (response) {
                let opt = new Option(response.text, response.id, true, true);
                select.append(opt).trigger('change')
            }
        });
    }
}

/**
 * initListeners: init event listener for select2
 * @param {string} selector
 * @param {object} select2
 * @param {object} events
 * @return {void}
 */
const initListeners = (selector, select2, events = {}) => {
    if (events && (typeof events === 'object')) {
        for (const key in events) {
            if (Object.hasOwnProperty.call(events, key) && (typeof events[key] === 'function')) {
                $(selector).on(key, (e) => events[key](select2));
            }
        }
    }
}

/**
 *
 * @param {string} selector // selector of select tag
 * @param {string} url  // data source repository where data is loaded from
 * @param {object} options // select2 options
 * @returns {object}
 */
window.createAjaxMasterSelect2 = (selector = ".select2", url, options = {}) => {
    let select = $(selector).select2({
        placeholder: options.placeholder ?? "Pilih",
        minimumInputLength: options.minimumInputLength ?? 3,
        ajax: {
            url: url,
            data: function (params) {
                let query = {
                    search: params.term
                }
                query = {
                    ...query,
                    ...options.query ?? {}
                };

                return query;
            },
            processResults: function (data) {
                return {
                    results: data.data
                };
            }
        }
    });

    select.dataSource = url;
    select.selectAnOption = () => selectAnOption(select);
    initListeners(selector, select, options.events);

    return select;
}

