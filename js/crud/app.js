
/************************************
 * Required Dependencies loading
 ************************************/

require('jquery-validation');
window.toastr = require('toastr');

/************************************
 * Modules loading
 ************************************/

// client side form validation
require('./validation');

// notification handler using toastr
require('./notification');

// CRRUD Ajax Handler
require('./crud');
