const submit = (el, setings = {}) => {

    let data = $(el.currentTarget).serialize();
    let url = $(el.currentTarget).attr('action');
    let method = $(el.currentTarget)
        .children('input[name=_method]')
        .val() ?? $(el.currentTarget).attr('method');

    let defaultSetings = {
        type: method,
        url: url,
        data: data,
        dataType: 'json',
        beforeSend: (xhr, settings) => disableSubmitButton(el, true),
        complete: (xhr, settings) => disableSubmitButton(el, false),
        error: (xhr, status, error) => errorHandler(el, xhr, status, error),
        success: (response) => successHandler(el, response),
    }

    setings = {
        ...defaultSetings,
        ...setings
    }

    $.ajax(setings);
}

const disableSubmitButton = (el, disabled = false) => {
    $(el.currentTarget).children('[type=submit]').attr('disabled', disabled);
}

const validationError = (response) => {
    let errors = response.errors;
    for (const field in errors) {
        if (Object.hasOwnProperty.call(errors, field)) {
            errors[field] = errors[field].join(', ');
        }
    }
    formValidation.showErrors(errors);
}

const errorHandler = (el, xhr, errorStatus, errorMessage) => {
    let { responseJSON, status } = xhr;
    if (status == 422) {
        return validationError(responseJSON)
    }
    return errorStd(responseJSON.message ?? errorMessage);
}

const successHandler = (el, response) => {
    let message = response.message ?? "Success";
    $(el.currentTarget).trigger('reset');
    return successStd(message);
}

const successStd = (message) => {
    notify(message);
}

const errorStd = (message) => {
    notify(message, 'error');
}

let formValidation = null;

$(() => {

    formValidation = $('[auto-form]').validate();

    $('[auto-form]').on('submit', (el) => {
        el.preventDefault();

        if (formValidation.form()) {
            submit(el);
        }
    });
})
